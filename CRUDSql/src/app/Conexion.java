/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package app;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;


public class Conexion {
    
    public static Connection establecerConnection(){
        try {
            String url = "jdbc:sqlserver://localhost:1433;"
                    + "database=escuelaRegistro;"
                    + "user=sa;"
                    + "password=chuc2710;"
                    + "loginTimeout=30;";
           Connection con  = DriverManager.getConnection(url);
             //JOptionPane.showMessageDialog(null, "se conecto a la base de datos ");
             return con;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se conecto a la base de datos con error: "+e.toString());
            return null;
        }
        
    }
    
    
}
